# GTOSS Reference Documents

See the GTOSS source code repository at
https://gitlab.com/rbarkmore/GTOSS

Example title page, taken from 'Quick Reference manual':

QUICK REFERENCE MANUAL (Corresponding to GTOSS Version H.10 Release)
Oct 2004
Developed By
  David D. Lang Associates
  Seattle, WA (206) 236 2579
Originally for
  NASA Johnson & Marshall Space Centers
Then On-going for
  Various Industry Advocates & Supporter
